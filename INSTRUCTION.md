---
title:  'Assignment 1'
author:
- Maja Frydrychowicz, Jaya Nilakantan, Patricia Campbell
- 420-520-DW
---

This assignment may be done individually or with a partner. The rubric, if you choose to work with a partner, will be more strict. Your solution must not be shared or published. You must inform your teacher of the partnership __before starting__.

__Deadline__: see Moodle. In case of _exceptional_ circumstances you may request
an extension by sending a MIO to your teacher. Otherwise, late submissions are 
not accepted.

_Read all the assigment instructions before you begin_.

# Introduction

In this assigment, you will:

* Modify code (see starter code repo) to improve readability and performance.
* Use browser developer tools and other tools to evaluate the performance 
  characteristics of the code before and after your improvements. 
* Write a brief report about the performance problems and fixes.

# General Requirements

## git and GitLab

If you are working with a __partner__:

* inform your teacher. Note: in future assignments, you may be required to work with other partners.
* your teacher will create a new subgroup for you and your partner
* fork the starter code into the new subgroup
* Use the Protected Branch Workflow. This means:
  * Don't make any commits on `main` or merge into `main`. To ease grading, __please call your staging branch `A1`__ 
  * Work on feature branches from A1. When one partner (the author) has finished a feature, open a Merge Request with details on what was done. The other partner (the reviewer) must review the changes, give meaningful feedback, and approve the MR. The author will then merge into A1.
  * Push your code to `origin` regularly to back-up your work.

If you are working __alone__:

* fork the starter code into the your 520-F2022/section0x/studentid subgroup 
* Use the Protected Branch Workflow. This means:
  * Don't make any commits on `main` or merge into `main`.
  * Work on a feature branch. To ease grading, __please call your branch `A1`__
  * Push `A1` to `origin` regularly to back-up your work.

In all cases, see the __Submission__ instructions at the end of this document. You will open a Merge Request on GitLab from A1 to main. 
* Make sure your Merge Request has a clear title.
* In the MR description, write down any challenges you faced; explain if there
  are any parts missing in your submission and why.
* Remember to follow the merge etiquette:
  * the author(s) of the assignment will submit the MR
  * your teacher will approve (or not)
      * optional, but recommended for your learning: make any necessary changes
      * optional: you may merge to main once the changes are complete

### Commit Style

Ideally you should make many small commits that record the progress of your
work at logical steps. 

Your commits messages should be short and descriptive. Use a consistent writing 
style. 

It is a widely adopted practice to start commit messages with an 
_[imperative verb](https://www.grammarly.com/blog/imperative-verbs/)_, an action, so that each message sounds like a *command* or a step in an
algorithm. This makes the history easier to read when useing `git log` and other tools.

Good:

* "List all passengers by name."
* "Remove multiplication method."

Bad:

* "Stuff for Assignment 1" _(Does not start with imperative verb, nor describe anything)_
* "event handler" _(Not a sentence, and no action )_
* "Removed multiplication method" _(Does not start with imperative verb)_
* "Replace all click handlers in the code with just one handler on a parent element." _(More than 50 characters long)_

If you need to write a longer commit message, the first line should be a short
summary, following by more details. Example:

```
Refactor all click handlers

There were too many click handlers attached on individual elements
all over the codebase. This slows down the code and make changes
more difficult.

Instead we can attach a handler just once on a parent element.
```

👉 You can use `git commit --amend` to modify your most recent commit, including
the message. If you modify commits that you've already pushed to a remote, you'll
have to (force) `git push -f origin mybranch` to update the branch remotely.

## Code Style

The starter repo contains a file called `.eslintrc.json` that
specifies the code style required in this course. In the
school labs, VSCode is configured with the `ESLint` extension and should
automatically warn you about code style issues. (See View > Problems)

For example, the following code should cause ESLint to report problems (at least 3 errors, 4 warnings) after you save the js file, if your VSCode is configured properly:

```js
if ( Math.floor(Math.random()*2) == 0) {
    image.style.background = "url(./labradoodle.PNG)";
    rand_image = "labra";
  } 
else {
  image.style.background = 'url(./fried-chicken.PNG)';
  randImage = "chicken";
}
```

You can run eslint at a VSCode terminal or bash/gitbash command line at any time `eslint myfile.js`
 
You can use ESLint to automatically fix most problems. Either `eslint --fix`
at the command-line or, in VSCode hit `Ctrl+Shift+P` and search for "ESLint: Fix
all autofixable problems". 

# Specification

## Code Review and fixes

Look at the (horribly written) code. This is good practice when you are hired in a company and need to support someone else's horribly written code 😁 But unlike other developers, we have given you hints in the comments of what you should be fixing.

Fix all the functions that are not working.

Rewrite code that is badly written.

## Report

As you follow the rest of this assignment spec, write a report in a file 
called `Performance.md`
using [Markdown syntax](https://www.markdownguide.org/basic-syntax/)
syntax. (Markdown is automatically coverted to HTML on GitLab and GitHub, by the
way.) 

If you are working with a partner (that your teacher has approved) put both names in your report. 

Feel free to add screenshots or diagrams if you wish. (Adding images to Markdown
works very much like including images in HTML.)

Your report should contain a summary of what you learned about the performance of
the website. Here is a template:

```
# Performance of Dawson Comp Sci App
## Contributors
<!-- name of partners, if applicable-->
## Introduction and Methodology

<!-- Briefly state how you manually tested the app, and in what environment 
(which browsers, what browser versions, what kind of device, OS,
width and height of viewport as reported in the console with `window.screen) -->

## Areas to Improve

### Functionality

<!-- What you notice from initial manual testing: what works, what is broken? -->

### Network

#### No Throttling

#### Slow 3G

## Summary of Changes and impact on performance
2
## Results and Conclusion

```

## Get to know the application

__Before you begin, take note of your testing environment (see report template above).__

Open the application through VSCode's LiveServer. Are there error messages on the console? Are there issues in the network tab? Note any issues that you notice. Then try to fix them.

## Using Performance tools

Once you have fixed the code, open the page using the LiveServer extension on VSCode in an incognito window. Open Dev Tools, detach but make sure the browser window is in the foreground (you might need to resize your windows to half-width).

1. Use the Lighthouse tab to generate a Performance report for a Mobile device, simulating throttling. Write about on the performance metrics and areas to improve in your report.

2. Use Chrome Devtools Network panel with cache disabled. Look at the Network activity and note the waterfall and the DOMContentLoaded and Load times. Try scrolling on the page to see how the images are being loaded. Note any usability concerns.

Now clear the Network tab (the no-entry icon near the top left), throttle for a slow 3G network and reload. Repeat the steps above (note the waterfall, and scroll the page to assess the usability) Are there network requests that are downloaded after the load event?  

3. Use Chrome Devtools Performance panel. This tool may be sloooow, so check Disable JavaScript samples but keep the Screenshots checked. Keep network and CPU at no throttling. Click on the reload icon to start profiling and reload the page; then go to the page and scroll down, then return to Dev Tool to end the profile. This will take around a few seconds.

Move your mouse from left-to-right over the screenshots: did you capture your scrolling? If not clear this profile (the no-entry icon) and restart.

__Save the profile (Down Arrow) in json format and add it to your git branch in the same directory
as your report__. If you need to pause your work, you can re-open this json file in the Performance
panel at a later time.

Are there times when frames are dropped? Is you CPU running at close to 100%? If so, zoom in on these areas. What are the activities on the main thread? Explain why you think the CPU is maxed out?

## Performance Improvements

You can improve performance on an image-heavy site by resizing and optimizing the images. We will use a tool called [`cwebp`](https://developers.google.com/speed/webp/docs/cwebp) which has been installed on the lab systems. 

* the dimension of the card images are much too big for what we need. They can be resized to a max width of 640 px. Why? Because the `grid-template-columns` style in `cards.css` indicates the minimum width is 320 px; anything more that 2 x minimum width will be split into 2 columns on the grid. (FYI, [CSS Grid Layouts](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout) are a simple way to style a section into a grid of columns and rows). Steps to follow:

* looking at the [`cwebp`](https://developers.google.com/speed/webp/docs/cwebp) documentation, resize the images. Notice that the resulting filetype will be webp, the file format developed by Google for smaller files.
* You will see that your `images` folder contains much smaller images!
* Run the Lighthouse report again and note any changes in your report.


# Submission

There are steps to be done _both on GitLab and Moodle_.

On git/GitLab:

* Make sure ESLint shows no errors or warnings in your source code.
* Push your `A1` branch to your fork (origin).
* On GitLab, open a Merge Request of `A1` against `main`.
* Set your 520 teacher as reviewer.

On Moodle, Assignment 1:

* Submit a URL to the _Merge Request_ you created above.

You will receive a grade and feedback on Moodle. Your teacher may also make
detailed comments on your Merge Request.
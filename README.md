# Performance of Dawson Comp Sci App

## Introduction and Methodology
<!-- Briefly state how you manually tested the app, and in what environment 
(which browsers, what browser versions, what kind of device, OS,
width and height of viewport as reported in the console with `window.screen) -->
##### Environment
## Firefox
I opened the index.html file in the live server and started testing it.
appCodeName: "Mozilla"
appName: "Netscape"
appVersion: "5.0 (Windows)"
oscpu: "Windows NT 10.0; Win64; x64"
platform: "Win32"
userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:104.0) Gecko/20100101 Firefox/104.0"
###### The width and Height
colorDepth: 24​
height: 864
pixelDepth: 24
width: 1536

## Areas to Improve

### Functionality
First, while looking at the project files I see a lot of images that seem
to be unused with bad naming. The Javascript file has a lot of code with little comments. Possibly the courses array coudlve been seperated from the code
Second, when the page loads we see some code in the background which is 
very unsafe for the webpage, however after looking in the folder strucutre I think that code is actually an Image. On the footer, there is a toggled list of Semester, Course Number and Name which has no functionality ( broken code/no functionality was coded). Also, when the browser reloads the part under the animated image jumps down to the bottom of the page.
We see some console errors.
![image](./reportImages/consoleErrorsFirstLoad.png)

The network has two 404 status.
1. It is the favicon error which isn't important.
2. There is a fetch 404 status which is a problem since our fetch doesn't  get the information it needs.s
One thing to note is that loading the dawson_banner.png takes the most time to load in our website which causes jumps of the page content.

There is a temp.jpg image where the banner loads which is why we see some code when it first loads which is in fact an image.

## BEFORE Code Changes

### Peformance Panel
1. There are some Dropped frames, looks like when the animation loaded there was tiny scrolling happening, probably the jumpy effect which made frames to drop.

### Network/No Throttling
1. DOMContentLoaded takes 41ms.
2. Images are loaded via either coded in the html or with javascript.
3. There is two temp.jpg - one is loading it with js the other with img tag.
4. DawsonBanner imaage takes "a lot of time" for the server to respond - 214ms.
5. There is something called ws which is injected by Live Server.
6. Stable usibity.
![image](./reportImages/BeforeChangesNetwork.png)

### Slow 3G
1. I feel like Scrollability feels slower.
2. Content Downloading now take a lot more time for images.
3. DawsonCompSci/ document gets stalled for 19ms.
4. There is Signifcant time  difference for any file.
5. DOMContentLoaded is around 40-50 times slower.
![image](./reportImages/BeforeChangesNetowrkSlow3G.png)

### Mobile Performance
1. With a score of 96 for performance, Cumulative Layout Shift is orange at 0.153 ms.
2. In Opportunties it mentions that I would save 6s if I ever to conver the PNG/JPEG into WebP/AVIF which provides better compression (0range).
3. It also want for image elements to have a fix width/height (red).
4. It also want a viewport tag (red).
5. It want me to reduce network payload. I have 2719 KiB of network payload. (orange)

## AFTER Code Changes

### Peformance Panel
1. With a score of 100 for pefromance, evry metric is in green and is better than before the code changes happened.
2. The Analysis wants me to properly size images because it would save signifcant amount of time. (Check Image)
3. Like Before the Changes, it still wants me to serve images in WebP/AVIF formats.
4. It also wants me to efficiently enocde images.
5. There is also a little improvement to be done on eliminating render-blocking resources  and enabling text compression however it is done in the server.
6. Like Before the Changes, It also want for image elements to have a fix width/height (red).
7. Like Before the Changes, It also want a viewport tag (red).
8. Like Before the Changes, It wants me to reduce network payload. I have 54842 KiB of network payload. (red)
![image](./reportImages/PerformanceAfterCodeChanges.png)

### Network
### No Throttling
1. It takes a lot of time for dawson_banner.jpg to get a response from server for a single image, it takes 471ms nearly 1second.
2. IT take 382s for DOMContentLoaded.
3. It takes 787 ms for Load.
4. For the last whole 400ms only the browser is focused on getting the images from the server.
5. There is a lot of queueing due to all images being get at the same time but being loaded seperatly which makes it slower. The image quality and size has an effect on this. As the content Download takes time the others wait for the previous image to download which causes a long wait time.
![image](./reportImages/NetworkNoThrottleAfterChanges.png)
#### Slow 3G
1. It took a lot of time for everything to load.
2. It took 6.31 seconds for DOMContent loaded. All the necessary features were present only the images for each course card wasn't.
3. It took 11.7 minutes for Load.
4. There is one request that continued after the Load event, but I think that is due to live server. It is a websocket.
5. It took very long for images to finish their download. Ranging from 2minutes to 9 minutes.
![image](./reportImages/Network3GSlowAfterChanges.png)


## Summary of Changes and impact on performance
### Performance Panel 
1. The CPU usage in task manager went to 61%. I think this is because of the images trying to be downloaded.
2. There were 66.7 ms were frames were dropped. The frames were dropped when I scrolled to the bottome of the courses where the courses werent loaded yet and went back to to where courses where showing. Since the courses didn't exist and were loading in it dropped frames due to fast scrolling up and down. Also, it didnt snap the website since nothting shows up in the frames section however in its summary its a pure white page.
3. The cpu usage is very high due to it downloading and trying to display the images. There are continous back and forth, task and Composite layers, running on the main thread. Also, I noticed that the compositor is working on those two specific "events".
![image](./reportImages/PerformanceAfterCodeChanges.png)

## Lighthouse report after Performance Improvements
1. I have a score of 100%.
2. The images are now taking less space and are downloading much faster.
3. The report shows that render-blocking resources were reduced for 0.52seconds to 0.17seconds.
4. It shows that the images are now properly sized.
5. It shows that they are efficently encoded and are in the right format.
6. It shows that the images are now reduced in payload and that they have an explicit width and height.
7. First Contenful Paint and Largest Conteniful Paint took less time.

![image](./reportImages/LighthouseAfterPerformanceUpdate.png)


## Changes Made To Fix Code
1. setTimeout was used to wait for DOM Loading. I changed it to DOMContentLoaded.
2. Course images were getting assigned to the course Object and not the courses themselves,   hence I changed the loop for assignment of the images by changinge courses.image to              courses[i].image
3. Changed sortCourses() from using loops to array.sort()
4. Changed showCourses(), addCard() and setHandlers() from for loop to array.forEach()
5. Removed multiple changes query searches and added one eventLisetner to the parent.
6. Added more parameters to displayDescription() to lessen number of element queries
7. Refactored closing button event listener and put it on the parent ( description )
8. Changed image assignment for courses from a for loop to array.forEach()
9. Changed javascrpt animation to CSS animation - got to get teacher approval
10. Remove dynamically assigning dawson image to bnner
11. FIxed banner loading making website jumpy
12. Removed fetch for images since you can add the card.image into the article innerhtml directly and remove the extra request to be done.
13. Add functionality for select tag options for sorting, everytime you sort content gets reset and redisplayed.
14. Downloaded dawson_banner image to local.
15. Changed Course images to 640x320 webp.
16. Added Viewport meta tag.
17. Added fixed image size for courses.

## Results and Conclusion
In the end, I reduced and optomized the website to the best of my capabilties. While also optomizing after some of the tasks in README.md was asked. This lab helped improving the way I see a website. I know understand that there is more to a website than to just make it. We need to think of how to organize our code and how it will affect the performance for our users.
The website now runs really smooth on a deskptop or on a phone and it is quite fast.